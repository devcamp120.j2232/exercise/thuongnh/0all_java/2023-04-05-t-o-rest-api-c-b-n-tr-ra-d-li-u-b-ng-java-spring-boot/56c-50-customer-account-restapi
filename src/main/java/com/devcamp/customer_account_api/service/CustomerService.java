package com.devcamp.customer_account_api.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.customer_account_api.model.Customer;


@Service
public class CustomerService {
    // tạo ra 3 đối tượng khách hàng 
    Customer customer1 = new Customer(1, "thuong", 20);
    Customer customer2 = new Customer(2, "duong", 50);
    Customer customer3 = new Customer(3, "luong", 90);

    // phương thức trả về Arrlisst khách hàng 
    public ArrayList<Customer>  getCustomerArray(){
        ArrayList<Customer> customers = new ArrayList<Customer>();
        customers.add(customer1);
        customers.add(customer2);
        customers.add(customer3);
        return customers;
    }
}
