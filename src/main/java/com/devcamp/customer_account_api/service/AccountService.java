package com.devcamp.customer_account_api.service;

import java.util.ArrayList;
import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.customer_account_api.model.Account;

@Service
public class AccountService {
    @Autowired
    private CustomerService customerService;
    // khởi tạo 3 đối tượng tài khoản
   

    public ArrayList<Account> getCustomerArrList() {

        Account account1 = new Account(1, customerService.customer1, 120);
        Account account2 = new Account(2, customerService.customer2, 130);
        Account account3 = new Account(3, customerService.customer3, 140);
        // khởi tạo đối tượng ARR
        ArrayList<Account> accountARR = new ArrayList<Account>();
        // thêm các đối tượng vào mảng
        Account[] arrString = { account1, account2, account3 };
        // thêm đối tượng vào arrlisat
        accountARR = new ArrayList<Account>(Arrays.asList(arrString));
        return accountARR;

    }
}
