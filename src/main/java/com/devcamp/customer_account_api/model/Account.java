package com.devcamp.customer_account_api.model;

import java.text.DecimalFormat;

public class Account {
    private int id;
    private Customer customer;
    private double balance;
    public Account(int id, Customer customer, int balance) {
        this.id = id;
        this.customer = customer;
        this.balance = balance;
    }
    public Account(int id, Customer customer) {
        this.id = id;
        this.customer = customer;
    }
    public int getId() {
        return id;
    }
    public Customer getCustomer() {
        return customer;
    }
    public double getBalance() {
        return balance;
    }
    public void setBalance(int balance) {
        this.balance = balance;
    }
    @Override
    public String toString() {
        DecimalFormat df = new DecimalFormat("#.##");    // làm tròn 2 chữ số thập phân
        return "Account [id=" + id + ", customer=" + customer + ", balance=" + df.format(balance) + "]";
    }
    public String getCustomerName() {
        return customer.getName();
    }
    public Account deposit(double amount) {
        this.balance = balance + amount ;
        return this;
    }

    public Account withdraw(double amount) {
        if (this.balance >= amount) {
          this.balance -= amount;
        } else {
          System.out.println("amount withdrawn exceeds the current balance!");
        }
        return this;
      }
    

    
    
    
}
