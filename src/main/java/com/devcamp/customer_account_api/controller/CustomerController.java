
package com.devcamp.customer_account_api.controller;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import com.devcamp.customer_account_api.model.Account;
import com.devcamp.customer_account_api.model.Customer;
import com.devcamp.customer_account_api.service.AccountService;
import com.devcamp.customer_account_api.service.CustomerService;

@CrossOrigin // Java @CrossOrigin: cho phép CORS trên RESTful web service.
@RestController
public class CustomerController {

    @Autowired
    private CustomerService customerService;
    @GetMapping("/customers")
    public ArrayList<Customer> getAllCustomer() {
        ArrayList<Customer> accountsArr = customerService.getCustomerArray();
        return accountsArr;
    }
}
