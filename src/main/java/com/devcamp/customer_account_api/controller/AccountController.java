package com.devcamp.customer_account_api.controller;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import com.devcamp.customer_account_api.model.Account;
import com.devcamp.customer_account_api.service.AccountService;

@CrossOrigin // Java @CrossOrigin: cho phép CORS trên RESTful web service.
@RestController
public class AccountController {

    @Autowired
    private AccountService accountService;
    @GetMapping("/accounts")
    public ArrayList<Account> getAllAccount() {
        ArrayList<Account> accountsArr = accountService.getCustomerArrList();
        return accountsArr;
    }
}
